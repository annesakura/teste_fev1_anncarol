<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empresa".
 *
 * @property int $idempresa
 * @property string $razao_social
 * @property string $cnpj
 * @property string $nome_responsavel
 * @property string $cpf_responsavel
 * @property string $endereco
 * @property string $bairro
 * @property string $cep
 * @property string|null $complemento
 * @property string $cidade
 * @property string $estado
 */
class EmpresaModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empresa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['razao_social', 'cnpj', 'nome_responsavel', 'cpf_responsavel', 'endereco', 'bairro', 'cep', 'cidade', 'estado'], 'required'],
            [['cidade', 'estado'], 'string'],
            [['razao_social', 'nome_responsavel', 'endereco', 'complemento'], 'string', 'max' => 150],
            [['cnpj'], 'string', 'max' => 25, 'min' => 18],
            [['cpf_responsavel'], 'string', 'max' => 15, 'min' => 13],
            [['bairro'], 'string', 'max' => 50],
            [['cep'], 'string', 'max' => 10, 'min' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idempresa' => Yii::t('app', 'Id da empresa'),
            'razao_social' => Yii::t('app', 'Razão social'),
            'cnpj' => Yii::t('app', 'Cnpj'),
            'nome_responsavel' => Yii::t('app', 'Nome do responsável'),
            'cpf_responsavel' => Yii::t('app', 'Cpf do responsável'),
            'endereco' => Yii::t('app', 'Endereço'),
            'bairro' => Yii::t('app', 'Bairro'),
            'cep' => Yii::t('app', 'Cep'),
            'complemento' => Yii::t('app', 'Complemento'),
            'cidade' => Yii::t('app', 'Cidade'),
            'estado' => Yii::t('app', 'Estado'),
        ];
    }
}
