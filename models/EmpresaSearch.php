<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EmpresaModel;

/**
 * EmpresaSearch represents the model behind the search form of `app\models\EmpresaModel`.
 */
class EmpresaSearch extends EmpresaModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idempresa'], 'integer'],
            [['razao_social', 'cnpj', 'nome_responsavel', 'cpf_responsavel', 'endereco', 'bairro', 'cep', 'complemento', 'cidade', 'estado'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EmpresaModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idempresa' => $this->idempresa,
        ]);

        $query->andFilterWhere(['ilike', 'razao_social', $this->razao_social])
            ->andFilterWhere(['ilike', 'cnpj', $this->cnpj])
            ->andFilterWhere(['ilike', 'nome_responsavel', $this->nome_responsavel])
            ->andFilterWhere(['ilike', 'cpf_responsavel', $this->cpf_responsavel])
            ->andFilterWhere(['ilike', 'endereco', $this->endereco])
            ->andFilterWhere(['ilike', 'bairro', $this->bairro])
            ->andFilterWhere(['ilike', 'cep', $this->cep])
            ->andFilterWhere(['ilike', 'complemento', $this->complemento])
            ->andFilterWhere(['ilike', 'cidade', $this->cidade])
            ->andFilterWhere(['ilike', 'estado', $this->estado]);

        return $dataProvider;
    }
}
