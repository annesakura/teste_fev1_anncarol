<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EmpresaModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="empresa-model-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-sm-8">
            <?= $form->field($model, 'razao_social')->textInput(['maxlength' => true]) ?>
        </div>
        
        <div class="col-sm-4">
            <?= $form->field($model, 'cnpj')->textInput(['maxlength' => true])->widget(\yii\widgets\MaskedInput::className(), ['mask' => '99.999.999/9999-99']) ?>
        </div>
        
        <div class="col-sm-8">
            <?= $form->field($model, 'nome_responsavel')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-sm-4">
            <?= $form->field($model, 'cpf_responsavel')->textInput(['maxlength' => true])->widget(\yii\widgets\MaskedInput::className(), ['mask' => '999.999.999-99']) ?> 
        </div>

        <div class="col-sm-6">
            <?= $form->field($model, 'endereco')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-sm-3">
            <?= $form->field($model, 'bairro')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-sm-3">
            <?= $form->field($model, 'cep')->textInput(['maxlength' => true])->widget(\yii\widgets\MaskedInput::className(), ['mask' => '99.999-999']) //?>
        </div>

        <div class="col-sm-6">
            <?= $form->field($model, 'complemento')->textInput(['maxlength' => true]) ?>
        </div>

        <div class="col-sm-3">
            <?= $form->field($model, 'cidade')->textInput() ?>
        </div>

        <div class="col-sm-3">
            <?= $form->field($model, 'estado')->textInput() ?>
        </div>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Salvar'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
