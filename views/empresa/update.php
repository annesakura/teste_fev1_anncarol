<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EmpresaModel */

$this->title = Yii::t('app', 'Editar empresa: {name}', [
    'name' => $model->razao_social,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Empresa'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->razao_social, 'url' => ['view', 'id' => $model->idempresa]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="empresa-model-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
